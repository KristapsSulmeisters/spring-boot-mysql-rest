package com.example.mysqlRest.model.enums;

public enum Language {
	EN("en"), LT("lt"), RU("ru"), LV("lv"), ET("et"), FI("fi"), DE("de");

	private String label;

	private Language(String label) {
		this.label = label;
	}

	public String getId() {
		return this.name();
	}

	public String getName() {
		return this.getLabel();
	}

	public String getLabel() {
		return this.label;
	}
}
