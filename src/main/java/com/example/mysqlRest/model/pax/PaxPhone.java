package com.example.mysqlRest.model.pax;

import com.example.mysqlRest.model.PhoneType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@Table(name = "pax_phones",
		uniqueConstraints =
		@UniqueConstraint(columnNames = {"passenger_id", "phone_type_id"}))
@EqualsAndHashCode(callSuper=true)
public class PaxPhone extends ManyToOneLinkedToPassenger implements LinkedToPassenger {
	@Column(name = "dialing_code")
	private String dialingCode;

	private String number;

	@ManyToOne
	@JoinColumn(name = "phone_type_id", nullable = false)
	private PhoneType type;
}
